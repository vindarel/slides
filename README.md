# Librelois slides

This repository contains all* the slides of the presentations and conferences of `librelois` in relation to libre money.

Ce dépôt regroupe toutes* les slides des présentations et conférences de librelois en rapport avec la monnaie libre.

*since 2019 / depuis 2019

## Liste des présentations

### Rustkathon mars 2019

* [Présentation de l'écosystème logiciel de la G1](https://librelois.duniter.io/slides/rustkathon2019_1/g1-ecosystem/)
* [Présentation de webassembly](https://librelois.duniter.io/slides/rustkathon2019_1/wasm/)
* [Le protocole DUP](https://librelois.duniter.io/slides/rustkathon2019_1/dup-protocol/)
* [Le projet DURS (DUniter-RuSt)](https://librelois.duniter.io/slides/rustkathon2019_1/durs/)